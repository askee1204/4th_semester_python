from scapy.all import *
import pickle
import os
import time
from networkx.drawing.nx_agraph import graphviz_layout, to_agraph
import networkx as nx
from dbClasses import redisWrapper
import winsound

try:
    import pygraphviz
    from networkx.drawing.nx_agraph import write_dot
    print("Using package pygraphviz")
except ImportError:
    try:
        import pydot
        from networkx.drawing.nx_pydot import write_dot
        print("Using package pydot")
    except ImportError:
        raise
print('Testing DB - redis')
conn = redisWrapper("localhost", 32768)
conn.connectRedis()
fileName = "ucn"

print('Done importing, now to work')
start = time.time()
print('Work work')
a = dict()
if not os.path.isfile(fileName + '.pickle'):
    print("NO PICKLE :(")
    for p in PcapReader(fileName + '.pcapng'):
        if IP in p:
            ipSrc = str(p[IP].src)
            ipDst = str(p[IP].dst)
            length = len(p)
            if not ipSrc in a:
                a[ipSrc] = dict()
                a[ipSrc] = {ipDst : length}
            elif ipDst not in a[ipSrc]:
                a[ipSrc].update({ipDst : length})
            else:
                _prevLength = int(a[ipSrc][ipDst]) + int(length)
                a[ipSrc][ipDst] = _prevLength
    pickle_out = open(fileName + '.pickle', 'wb')
    pickle.dump(a, pickle_out)
    pickle_out.close()
else:
    print("PICKLE!")
    pickle_in = open(fileName + '.pickle', 'rb')
    a = pickle.load(pickle_in)
finish = time.time() - start
print(f'Time spent leading pcap file: {(finish)}')
start2 = time.time()
G=nx.Graph()
G.clear()
for key, value in a.items():
    G.add_node(key)
    for ip, length in value.items():
        if not G.has_node(ip):
            G.add_node(ip)
        if not G.has_edge(key, ip):
            G.add_edge(key, ip, weight=int(length))
labels = nx.get_edge_attributes(G,'weight')
write_dot(G, "grid.dot")
finish = time.time() - start2
print(f'Time spent creating graph: {(finish)}')
print(a)
redisWrapper.insertData1(conn, a)
b = redisWrapper.getData1(conn)
redisWrapper.insertData2(conn, a)
redisWrapper.getData2(conn)
finish = time.time() - start

print(f'Time spent total: {(finish)}')
duration = 1000  # milliseconds
freq = 440  # Hz
winsound.Beep(freq, duration)