import redis
import time
import json

class redisWrapper:
    def __init__(self, host, port):
        self.host = host
        self.port = port
    def connectRedis(self):
        try:
            self.conn = redis.StrictRedis(
                host=self.host,
                port=self.port,
                db=0)
            print(self.conn)
            self.conn.ping()
            print('Connected!')
            return self.conn
        except Exception as ex:
            print('Error:', ex)
            exit('Failed to connect, terminating.')

    #"Source;destination" as key length as value
    def insertData1(self, data):
        start = time.time()
        for source, ips in data.items():
            for ip, length in ips.items():
                self.conn.set(str(source + ';' + ip), length)
        finnish = time.time() - start
        print("Done inserting oldschool. Time spent: " + str(finnish) + " on " + str(self.conn.dbsize()) + " keys")
        #self.conn.flushall()

    #Source as key destinations and lengths in JSON format as value
    def insertData2(self, data):
        start = time.time()
        for source, ips in data.items():
            self.conn.set(str(source), json.dumps(ips))
        finnish = time.time() - start
        print("Done inserting JSON as value. Time spent: " + str(finnish) + " on " + str(self.conn.dbsize()) + " keys")
        #self.conn.flushall()
    
    def getData1(self):
        start = time.time()
        i = 0
        a = dict()
        for key in self.conn.scan_iter():
            i = i + 1
            ipSrc, ipDst = str(key).split(';')
            length = self.conn.get(key)
            if not ipSrc in a:
                a[ipSrc] = dict()
                a[ipSrc] = {ipDst : length}
            elif ipDst not in a[ipSrc]:
                a[ipSrc].update({ipDst : length})
            else:
                _prevLength = int(a[ipSrc][ipDst]) + int(length)
                a[ipSrc][ipDst] = _prevLength

        finnish = time.time() - start
        print("Done getting data from: " + str(i) + " keys")
        print("Time spent: " + str(finnish))
        self.conn.flushall()
        return a
    
    def getData2(self):
        start = time.time()
        i = 0
        a = dict()
        for key in self.conn.scan_iter():
            a[key] = dict()
            a[key] = json.loads(self.conn.get(key))
            i = i + 1
        finnish = time.time() - start
        print("Done getting data from: " + str(i) + " keys")
        print("Time spent: " + str(finnish))
        self.conn.flushall()